-- -------------------------------------------------------------------------
-- banco_de_dados_teste_da_liberfly

DROP SCHEMA IF EXISTS banco_de_dados_teste_da_liberfly;

CREATE SCHEMA IF NOT EXISTS banco_de_dados_teste_da_liberfly 
DEFAULT CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

USE banco_de_dados_teste_da_liberfly;

-- -------------------------------------------------------------------------
-- Tabela usuario

DROP TABLE IF EXISTS usuario;

CREATE TABLE IF NOT EXISTS usuario(
  pk_usuario INT NOT NULL AUTO_INCREMENT,
  nome_de_usuario VARCHAR(80) NOT NULL,
  senha_para_web_service VARCHAR(120) NOT NULL,
  PRIMARY KEY (pk_usuario),
  UNIQUE INDEX nome_de_usuario_UNICA (nome_de_usuario ASC))
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Tabela documento

DROP TABLE IF EXISTS documento;

CREATE TABLE IF NOT EXISTS documento(
  pk_documento INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(250) NOT NULL,
  momento_do_registro DATETIME NOT NULL,
  link_para_download VARCHAR(1000) NOT NULL,
  PRIMARY KEY (pk_documento))
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Dados de exemplo:

-- Documentos:
INSERT INTO documento (nome, momento_do_registro, link_para_download) VALUES
('documento_um', '2023-05-21 14:01:00', 'https://endereco_do_download/documento_um'),
('documento_dois', '2023-05-21 14:02:00', 'https://endereco_do_download/documento_dois'),
('documento_tres', '2023-05-21 14:03:00', 'https://endereco_do_download/documento_tres'),
('documento_quatro', '2023-05-21 14:04:00', 'https://endereco_do_download/documento_quatro'),
('documento_cinco', '2023-05-21 14:05:00', 'https://endereco_do_download/documento_cinco'),
('documento_seis', '2023-05-21 14:06:00', 'https://endereco_do_download/documento_seis'),
('documento_sete', '2023-05-21 14:07:00', 'https://endereco_do_download/documento_sete'),
('documento_oito', '2023-05-21 14:08:00', 'https://endereco_do_download/documento_oito'),
('documento_nove', '2023-05-21 14:09:00', 'https://endereco_do_download/documento_nove'),
('documento_dez', '2023-05-21 14:10:00', 'https://endereco_do_download/documento_dez'),
('documento_onze', '2023-05-21 14:11:00', 'https://endereco_do_download/documento_onze'),
('documento_doze', '2023-05-21 14:12:00', 'https://endereco_do_download/documento_doze'),
('documento_treze', '2023-05-21 14:13:00', 'https://endereco_do_download/documento_treze'),
('documento_quatorze', '2023-05-21 14:14:00', 'https://endereco_do_download/documento_quatorze'),
('documento_quinze', '2023-05-21 14:15:00', 'https://endereco_do_download/documento_quinze'),
('documento_dezesseis', '2023-05-21 14:16:00', 'https://endereco_do_download/documento_dezesseis'),
('documento_dezessete', '2023-05-21 14:17:00', 'https://endereco_do_download/documento_dezessete'),
('documento_dezoito', '2023-05-21 14:18:00', 'https://endereco_do_download/documento_dezoito'),
('documento_dezenove', '2023-05-21 14:14:00', 'https://endereco_do_download/documento_dezenove'),
('documento_vinte', '2023-05-21 14:20:00', 'https://endereco_do_download/documento_vinte'),
('documento_vinte_um', '2023-05-21 14:21:00', 'https://endereco_do_download/documento_vinte_um'),
('documento_vinte_dois', '2023-05-21 14:22:00', 'https://endereco_do_download/documento_vinte_dois'),
('documento_vinte_tres', '2023-05-21 14:23:00', 'https://endereco_do_download/documento_vinte_tres'),
('documento_vinte_quatro', '2023-05-21 14:24:00', 'https://endereco_do_download/documento_vinte_quatro'),
('documento_vinte_cinco', '2023-05-21 14:25:00', 'https://endereco_do_download/documento_vinte_cinco'),
('documento_vinte_seis', '2023-05-21 14:26:00', 'https://endereco_do_download/documento_vinte_seis'),
('documento_vinte_sete', '2023-05-21 14:27:00', 'https://endereco_do_download/documento_vinte_sete'),
('documento_vinte_oito', '2023-05-21 14:28:00', 'https://endereco_do_download/documento_vinte_oito'),
('documento_vinte_nove', '2023-05-21 14:29:00', 'https://endereco_do_download/documento_vinte_nove'),
('documento_trinta', '2023-05-21 14:30:00', 'https://endereco_do_download/documento_trinta');
