<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WebServiceDocumentosController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider and all of them will
  | be assigned to the "api" middleware group. Make something great!
  |
 */

Route::post('servico_post_login', [WebServiceDocumentosController::class, 'servico_post_login'])->name('login');
Route::post('servico_post_refazer_login', [WebServiceDocumentosController::class, 'servico_post_refazer_login']);
Route::post('servico_post_quem_sou_eu', [WebServiceDocumentosController::class, 'servico_post_quem_sou_eu']);
Route::post('servico_post_logout', [WebServiceDocumentosController::class, 'servico_post_logout']);
Route::get('servico_get_documentos', [WebServiceDocumentosController::class, 'servico_get_documentos']);
Route::get('servico_get_documento', [WebServiceDocumentosController::class, 'servico_get_documento']);
