<?php

namespace Database\Seeders;

use App\Models\WebServiceDocumentosModel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder{

  public function run(): void{
    WebServiceDocumentosModel::create([
      'nome_de_usuario' => 'usuario',
      'senha_para_web_service' => bcrypt('senha')
    ]);
  }

}
