<?php

namespace App\Http\Controllers;

use App\Models\WebServiceDocumentosModel;

final class WebServiceDocumentosController extends TemplateController{

  public function __construct(){
    $excecoes_do_middleware[] = 'servico_post_login';
    $opcoes_do_middleware['except'] = $excecoes_do_middleware;
    $this->middleware('auth:api', $opcoes_do_middleware);
  }

  public function servico_post_login(){
    $credenciais['nome_de_usuario'] = request('usuario');
    $credenciais['password'] = request('senha');

    $token = auth()->attempt($credenciais);
    if(!$token){
      $retorno['mensagem_de_falha'] = 'Você não tem permissão para utilizar este serviço.';
      return response($retorno, 401);
    }

    return $this->retornar_token($token);
  }

  public function servico_post_refazer_login(){
    return $this->retornar_token(auth()->refresh());
  }

  public function servico_post_quem_sou_eu(){
    $retorno['id'] = auth()->user()->pk_usuario;
    $retorno['nome'] = auth()->user()->nome_de_usuario;
    return response($retorno, 200);
  }

  public function servico_post_logout(){
    auth()->logout();
    $retorno['mensagem_de_sucesso'] = 'Logout bem sucedido.';
    return response($retorno, 200);
  }

  private function retornar_token($token){
    $retorno['token'] = $token;
    $retorno['tipo_do_token'] = 'bearer';
    $retorno['duracao_do_token'] = auth()->factory()->getTTL() * 60;
    return response($retorno, 200);
  }

  public function servico_get_documentos(){
    $web_service_documentos_model = new WebServiceDocumentosModel();

    $documentos = $web_service_documentos_model->selecionar_documentos();
    $array_documentos = array();
    foreach($documentos as $documento){
      $array_documento = array();

      $id = $documento->get_pk_documento();
      $array_documento['id'] = $id;

      $nome_do_documento = $documento->get_nome();
      $array_documento['nome_do_documento'] = $nome_do_documento;

      $momento_do_registro = $documento->get_momento_do_registro();
      $momento_do_registro = $this->converter_para_horario_data_do_html($momento_do_registro);
      $array_documento['momento_do_registro'] = $momento_do_registro;

      $link_para_download = $documento->get_link_para_download();
      $array_documento['link_para_download'] = $link_para_download;

      $array_documentos[] = $array_documento;
    }

    $aviso = 'Se for utilizar os dados em um HTML, utilize a função htmlspecialchars do PHP para';
    $aviso .= ' impedir ataques XSS.';

    $retorno = array();
    $retorno['aviso'] = $aviso;
    $retorno['documentos'] = $array_documentos;

    return response($retorno, 200);
  }

  public function servico_get_documento(){
    $web_service_documentos_model = new WebServiceDocumentosModel();

    /* Validando o ID do documento informado na URL */
    $id_do_documento = request('id');
    if(!is_numeric($id_do_documento) or $id_do_documento <= 0 or floor($id_do_documento) != $id_do_documento){
      $mensagem = 'ID inválido, o ID do documento precisa ser um número natural maior que zero.';
      $retorno = array();
      $retorno['mensagem_de_falha'] = $mensagem;
      return response($retorno, 400);
    }else{
      /* Consultando informações do documento */
      $array_resultado = $web_service_documentos_model->selecionar_documento($id_do_documento);
      if(isset($array_resultado['mensagem_do_model'])){
        $retorno = array();
        $retorno['mensagem_de_falha'] = $array_resultado['mensagem_do_model'];
        return response($retorno, 404);
      }else{
        $documento = $array_resultado[0];

        $id = $documento->get_pk_documento();
        $array_documento['id'] = $id;

        $nome_do_documento = $documento->get_nome();
        $array_documento['nome_do_documento'] = $nome_do_documento;

        $momento_do_registro = $documento->get_momento_do_registro();
        $momento_do_registro = $this->converter_para_horario_data_do_html($momento_do_registro);
        $array_documento['momento_do_registro'] = $momento_do_registro;

        $link_para_download = $documento->get_link_para_download();
        $array_documento['link_para_download'] = $link_para_download;

        $aviso = 'Se for utilizar os dados em um HTML, utilize a função htmlspecialchars do PHP para';
        $aviso .= ' impedir ataques XSS.';

        $retorno = array();
        $retorno['aviso'] = $aviso;
        $retorno['documento'] = $array_documento;
      }
    }

    return response($retorno, 200);
  }

}
