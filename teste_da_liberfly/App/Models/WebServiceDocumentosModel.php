<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Entidades\Documento;

final class WebServiceDocumentosModel extends Authenticatable implements JWTSubject{
  protected $table = 'usuario';
  protected $primaryKey = 'pk_usuario';
  protected $hidden = ['senha_para_web_service'];
  public $timestamps = false;

  public function getAuthPassword(){
    return $this->senha_para_web_service;
  }

  public function getJWTIdentifier(){
    return $this->getKey();
  }

  public function getJWTCustomClaims(){
    return [];
  }

  public function selecionar_documentos(){
    $query = DB::table('documento');
    $query = $query->addSelect('pk_documento');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('momento_do_registro');
    $query = $query->addSelect('link_para_download');

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $valores = (array) $objeto_generico;
      $documento = new Documento($valores);
      $array_melhorado[] = $documento;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function selecionar_documento($id_do_documento){
    $query = DB::table('documento');
    $query = $query->addSelect('pk_documento');
    $query = $query->addSelect('nome');
    $query = $query->addSelect('momento_do_registro');
    $query = $query->addSelect('link_para_download');

    $query = $query->where('pk_documento', '=', $id_do_documento);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhum documento com ID $id_do_documento foi encontrado no banco de";
      $mensagem_do_model .= ' dados do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];
      $documento = new Documento($valores);
      $array_melhorado[] = $documento;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

}
