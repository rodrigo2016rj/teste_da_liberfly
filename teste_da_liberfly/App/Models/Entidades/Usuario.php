<?php

namespace App\Models\Entidades;

final class Usuario{
  private $pk_usuario;
  private $nome_de_usuario;
  private $senha_para_web_service;

  public function __construct($array_usuario = array()){
    if(isset($array_usuario['pk_usuario'])){
      $this->pk_usuario = $array_usuario['pk_usuario'];
    }
    if(isset($array_usuario['nome_de_usuario'])){
      $this->nome_de_usuario = $array_usuario['nome_de_usuario'];
    }
    if(isset($array_usuario['senha_para_web_service'])){
      $this->senha_para_web_service = $array_usuario['senha_para_web_service'];
    }
  }

  public function set_pk_usuario($pk_usuario){
    $this->pk_usuario = $pk_usuario;
  }

  public function set_nome_de_usuario($nome_de_usuario){
    $this->nome_de_usuario = $nome_de_usuario;
  }

  public function set_senha_para_web_service($senha_para_web_service){
    $this->senha_para_web_service = $senha_para_web_service;
  }

  public function get_pk_usuario(){
    return $this->pk_usuario;
  }

  public function get_nome_de_usuario(){
    return $this->nome_de_usuario;
  }

  public function get_senha_para_web_service(){
    return $this->senha_para_web_service;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome_de_usuario':
        return 2;
      case 'senha_para_web_service':
        return 9;
    }
    return -1;
  }

  //O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome_de_usuario':
        return 25;
      case 'senha_para_web_service':
        return 120;
    }
    return -1;
  }

}
