<?php

namespace App\Models\Entidades;

final class Documento{
  private $pk_documento;
  private $nome;
  private $momento_do_registro;
  private $link_para_download;

  public function __construct($array_documento = array()){
    if(isset($array_documento['pk_documento'])){
      $this->pk_documento = $array_documento['pk_documento'];
    }
    if(isset($array_documento['nome'])){
      $this->nome = $array_documento['nome'];
    }
    if(isset($array_documento['momento_do_registro'])){
      $this->momento_do_registro = $array_documento['momento_do_registro'];
    }
    if(isset($array_documento['link_para_download'])){
      $this->link_para_download = $array_documento['link_para_download'];
    }
  }

  public function set_pk_documento($pk_documento){
    $this->pk_documento = $pk_documento;
  }

  public function set_nome($nome){
    $this->nome = $nome;
  }

  public function set_momento_do_registro($momento_do_registro){
    $this->momento_do_registro = $momento_do_registro;
  }

  public function set_link_para_download($link_para_download){
    $this->link_para_download = $link_para_download;
  }

  public function get_pk_documento(){
    return $this->pk_documento;
  }

  public function get_nome(){
    return $this->nome;
  }

  public function get_momento_do_registro(){
    return $this->momento_do_registro;
  }

  public function get_link_para_download(){
    return $this->link_para_download;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 2;
      case 'link_para_download':
        return 10;
    }
    return -1;
  }

  //O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'nome':
        return 200;
      case 'link_para_download':
        return 800;
    }
    return -1;
  }

}
