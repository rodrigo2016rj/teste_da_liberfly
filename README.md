## Sobre
<p>Este sistema foi feito com PHP 8.2.0, Laravel 10.11.0, Tymon JWT Auth 2.0.0 e MySQL Server 8.0.28.</p>

<br/>

## Motivação
<p>O que me motivou a fazer este sistema foi:<br/>
Tentar ser aprovado em um processo seletivo de emprego.</p>

<br/>

## Instruções
<p>Para ver o resultado em um ambiente de desenvolvimento siga as instruções:</p>

<p>Inicie o MySQL Server.</p>

<p>Utilize o banco de dados contido no arquivo banco_de_dados_teste_da_liberfly.sql.</p>

<p>Configure o MySQL Server para que o banco de dados deste sistema seja acessado por username root sem senha.</p>

<p>Se você preferir, você pode configurar neste sistema um outro username e uma outra senha.</p>

<p>Configure seu PHP pelo arquivo php.ini e certifique-se de deixar ativado intl e mbstring.</p>

<p>Coloque o diretório teste_da_liberfly dentro do endereço DocumentRoot do seu Servidor Apache. Exemplo: coloque dentro de htdocs do XAMPP. Geralmente o DocumentRoot é o diretório htdocs do XAMPP e você pode consultar ou mudar o endereço de DocumentRoot pelo arquivo de configuração do Servidor Apache (exemplo: arquivo httpd.conf).</p>

<p>Configure um VirtualHost no Servidor Apache para este sistema.<br/>
Dica: configure com a porta 80 e ServerName localhost, se tiver dúvida procure algum tutorial.<br/>
Se utiliza XAMPP, o arquivo de configuração do Servidor Apache para VirtualHost será apache\conf\extra\httpd-vhosts.conf<br/>
Exemplo de VirtualHost configurado:<br/>
<code>&lt;VirtualHost *:80&gt;</code><br/>
<code>&nbsp;&nbsp;DocumentRoot "C:\Users\Rodrigo\Servidores\XAMPP001\htdocs\teste_da_liberfly\public"</code><br/>
<code>&nbsp;&nbsp;ServerName localhost</code><br/>
<code>&lt;/VirtualHost&gt;</code></p>

<p>Inicie ou reinicie o Servidor Apache.</p>

<p>Dentro do diretório teste_da_liberfly execute os comandos:<br/>
composer install (instala o Laravel e o Tymon JWT Auth)<br/>
php artisan db:seed (cria um usuário usuario cuja senha é senha)</p>

<p>Renomeie o arquivo .env.example para .env<br/>
Gere a chave APP_KEY do arquivo .env pelo comando: php artisan key:generate<br/>
Gere a chave JWT_SECRET do arquivo .env pelo comando: php artisan jwt:secret</p>

<p>Utilize o programa Postman para verificar o resultado, exemplos:</p>

<p>Method: POST<br/>
Endereço: http://localhost:80/api/servico_post_login?usuario=usuario&senha=senha<br/>
Header para adicionar: Nenhum</p>

<p>Method: GET<br/>
Endereço: http://localhost:80/api/servico_get_documentos?token=coloque_o_token_retornado_pelo_login_aqui<br/>
Header para adicionar: Accept application/json</p>

<p>Method: GET<br/>
Endereço: http://localhost:80/api/servico_get_documento?id=22&token=coloque_o_token_retornado_pelo_login_aqui<br/>
Header para adicionar: Accept application/json</p>

<p>Method: POST<br/>
Endereço: http://localhost:80/api/servico_post_refazer_login?token=coloque_o_token_retornado_pelo_login_aqui<br/>
Header para adicionar: Accept application/json</p>

<p>Method: POST<br/>
Endereço: http://localhost:80/api/servico_post_quem_sou_eu?token=coloque_o_token_retornado_pelo_login_aqui<br/>
Header para adicionar: Accept application/json</p>

<p>Method: POST<br/>
Endereço: http://localhost:80/api/servico_post_logout?token=coloque_o_token_retornado_pelo_login_aqui<br/>
Header para adicionar: Accept application/json</p>

<br/>
